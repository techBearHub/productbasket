package com.company;

public class Product {
    private String productName;
    private int productCount;

    public Product(String productName){
        this.productName = productName;
        productCount = 1;
    }

    public void addProduct(){
        productCount++;
    }

    public int getProductCount(){
        return productCount;
    }

    public void removeProduct(){
        if(productCount > 0)
            productCount--;
        else
            productCount = 0;

    }

    public String getProductName(){
        return productName;
    }


}
