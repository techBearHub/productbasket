package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        ArrayList<String> inStock = new ArrayList<>();
        ArrayList<Product> productList = new ArrayList<>();

        initProductList(inStock);

        String choice = "";

        Scanner sc = new Scanner(System.in);

        while(!choice.toLowerCase().equals("0")){


            System.out.println("Ko velaties darit?");
            System.out.println("1- Izvadit preces, kas ir pieejamas");
            System.out.println("2- Izvadit groza saturu");
            System.out.println("3- Pievienot grozam preci");
            System.out.println("4- Iznemt preci no groza");
            System.out.println("5- Pievienot jaunu preci noliktavai");
            System.out.println("0- Iziet");


            choice = sc.next();

            switch (choice){
                case "1":
                    printInStock(inStock);
                    break;

                case "2":
                    printBasket(productList);
                    break;

                case "3":
                    addProduct(inStock, productList);
                    break;

                case "4":
                    removeProduct(productList);
                    break;

                case "5":
                    addToStock(inStock);
                    break;

                case "0":
                    break;
            }

        }

    }

    private static void addToStock(ArrayList<String> inStock) {
        System.out.println("Kads ir preces nosaukums?");
        Scanner sc = new Scanner(System.in);
        String product = sc.next();

        inStock.add(product);
    }


    private static void initProductList(ArrayList<String> inStock) {
        //Lai butu vieglak- aizpildit sarakstu ar "default" projektiem
        //inStock ir String lists no kuriem tad ari vares taisit Product objektus :)
        inStock.add("Monitors Samsung");
        inStock.add("Monitors BenQ");
        inStock.add("Dators Lenovo");
        inStock.add("Pele Logitech");
        inStock.add("Dators Dell");
        inStock.add("Klaviatura IBM");
        System.out.println("--------------");

    }

    private static void printInStock(ArrayList<String> inStock){
        for(int i = 0; i < inStock.size(); i++){
            System.out.println((i+1) + ". " + inStock.get(i));
        }
        System.out.println("--------------");
    }


    private static void printBasket(ArrayList<Product> productList) {
        if(productList.isEmpty()){
            System.out.println("Saraksts ir tukss");
            System.out.println("--------------");
            return;
        }
        for (int i = 0; i < productList.size(); i++) {
            System.out.println((i+1) + ". Produkta nosaukums- " + productList.get(i).getProductName() + ", skaits- " + productList.get(i).getProductCount());
        }
        System.out.println("--------------");
    }

    private static void addProduct(ArrayList<String> inStock, ArrayList<Product> productList) {
        printInStock(inStock);
        System.out.println("Kuru produktu velaties pievienot? Ievadiet attiecigo skaitli");

        Scanner sc = new Scanner(System.in);
        String input = sc.next();

        if(input.matches("[0-9]+") ) {
            int index = Integer.valueOf(input);
            index--;

            if(index >= 0 && index < inStock.size()){
                addToBasket(productList, inStock.get(index));
                return;
            }

        }

        System.out.println("Produkts neeksiste!");
    }

    private static void addToBasket(ArrayList<Product> productList, String productName){

        for(Product product : productList){
            if(product.getProductName().equals(productName)){
                product.addProduct();
                System.out.println("Prece pievienota!");
                return;
            }
        }
        productList.add(new Product(productName));
        System.out.println("Prece pievienota!");
    }

    private static void removeProduct(ArrayList<Product> productList) {
        if(productList.isEmpty()){
            System.out.println("Saraksts ir tukss");
            return;
        }
        printBasket(productList);
        System.out.println("Kuru produktu velaties iznemt? Ievadiet attiecigo skaitli");

        Scanner sc = new Scanner(System.in);
        String input = sc.next();

        if(input.matches("[0-9]+") ) {
            int index = Integer.valueOf(input);
            index--;

            if(index >= 0 && index < productList.size()){
                removeFromBasket(productList, index);
                return;
            }

        }

        System.out.println("Produkts neeksiste!");


    }


    static void removeFromBasket(ArrayList<Product> productList, int toRemove){

        Product pr = productList.get(toRemove);
        pr.removeProduct();

        if(pr.getProductCount() < 1){
            productList.remove(toRemove);
        }

    }

}
